var ach = require('@ach/ach');

var test = ach().create({
  from: {
    name: "test",
    fein: "12345689",
  },
  for: {
    name: "Our Bank",
    routing: "123456789"
  },
})

console.log(test);

// Unable to get a generated output of the ach file
var results = ach().from(test).to('ach')
console.log(results);