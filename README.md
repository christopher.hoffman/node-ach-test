## A test for nacha packages.

Conclusion: No existing package seems to be regularly maintained. Use the `nach2` package, as long as it relying on an older version of moment.js isn't a deal breaker.

# nACH2
Homepage: https://github.com/glenselle/nACH2

Last Published 3 years ago.

`npm i nach2 --save`

Test with `node nach2.js`, and it'll put a file called nach2.txt with the output.

Looks to be a fork of the `nach` package, where maintainers of the original package delegated maintenance over to `nach2`.

Pros:
* Documentation is good.
* Takes in option overrides for most fields.
* Provides validation of most of the fields.

Cons:
* Relies on an older verson of moment.js, which throws a deprecation warning when used:
Deprecation warning: moment().add(period, number) is deprecated. Please use moment().add(number, period). See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info.


# @ach/ach
https://www.npmjs.com/package/@ach/ach

Last publish 5 years ago.

`npm install @ach/ach --save`


Written in coffeescript, so we can use the javascript, but there aren't any good examples in javascript for consuming this.

# NACHA 
https://github.com/bitfloor/nacha

Last published 9 years ago, doesn't support non 200 service class codes. Doesn't fit our need.