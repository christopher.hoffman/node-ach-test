/**
 * Testing the nach2 package by generating a sample nacha file.
 */
var nach = require('nach2');
var moment = require('moment');
var fs = require('fs');
var faker = require('faker');

var file = new nach.File({
  immediateDestination: '081000032',
  immediateOrigin: '123456789',
  immediateDestinationName: 'Some Bank',
  immediateOriginName: 'Your Company Inc',
  referenceCode: '#A000001',
});

var batch = new nach.Batch({
  serviceClassCode: '200',
  companyName: 'Your Company Inc',
  standardEntryClassCode: 'WEB',
  companyIdentification: '123456789',
  companyEntryDescription: 'Trans Description',
  companyDescriptiveDate: moment(nach.Utils.computeBusinessDay(8)).format('MMM D'),
  effectiveEntryDate: nach.Utils.computeBusinessDay(8),
  originatingDFI: '081000032',
  companyDiscretionaryData: 'Opt Discretionary',
});

var entry = new nach.Entry({
    receivingDFI: '081000210',
    DFIAccount: '5654221',
    amount: '175',
    idNumber: 'RAj##32b1kn1bb3',
    individualName: 'Luke Skywalker',
    discretionaryData: '  ',
    transactionCode: '22'
});

// Doesn't look like EntryAddenda works
// var addenda = new nach.EntryAddenda({
//   paymentRelatedInformation: "0123456789ABCDEFGJIJKLMNOPQRSTUVWXYXabcdefgjijklmnopqrstuvwxyx"
// });
// entry.addAdenda(addenda);

batch.addEntry(entry);

createRandomEntries(10, batch);

file.addBatch(batch);

// Generate the file (result is a string with the file contents)
file.generateFile(function(result) {

  // Write result to a NACHA.txt file
  fs.writeFile('nach2.txt', result, function(err) {
      if(err) console.log(err);

      // Log the output
      console.log(result);
  });
});

/**
 * Helper function to create a number of random entries.
 * @param {number} num 
 * @param {nach.Batch} batch 
 */
function createRandomEntries(num, batch) {
  
  for (let i = 0; i < num; i++) {
    let entry = createRandomEntry();
    batch.addEntry(entry);
  }
}

/**
 * Generate a single random entry.
 */
function createRandomEntry() {
  return new nach.Entry({
    receivingDFI: faker.finance.routingNumber(),
    DFIAccount: faker.finance.routingNumber(),
    amount: faker.finance.amount(0, 100),
    idNumber: faker.random.alphaNumeric(15),
    individualName: faker.fake("{{name.firstName}} {{name.middleName}} {{name.lastName}}"),
    discretionaryData: faker.random.alpha(2),
    transactionCode: '22'
});
}